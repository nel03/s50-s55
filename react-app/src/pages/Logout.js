import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Logout() {
    
    // localStorage.clear()
    const { unsetUser, setUser } = useContext(UserContext)

    // Function to clear user's information from the localStorage
    unsetUser();

	useEffect(() => {
		setUser({id: null})
	},[])
    
    return (
        <Navigate to="/"/>
    )
}