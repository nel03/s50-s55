import { Row, Col, Accordion, Button} from 'react-bootstrap'

export default function Resources() {
  return (
            <Row className="mt-3 mb-3">
                
                <Col xs={12} md={4}>
                    <Accordion className="p-3">
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>React Js</Accordion.Header>
                            <Accordion.Body>
                            React has been designed from the start for gradual adoption, and you can use as little or as much React as you need. Whether you want to get a taste of React, add some interactivity to a simple HTML page, or start a complex React-powered app, the links in this section will help you get started.
                            <br></br>
                            <Button className="d-flex align-items-center justify-content-center" href="https://reactjs.org/docs/getting-started.html" variant="link"> Read More..</Button>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Col>

                <Col xs={12} md={4}>
                    <Accordion className="p-3">
                        <Accordion.Item eventKey="1">
                            <Accordion.Header>JavaScript</Accordion.Header>
                            <Accordion.Body>
                            Welcome to the JavaScriptTutorial.net website! This JavaScript Tutorial helps you learn the JavaScript programming language from scratch quickly and effectively.

                            JavaScript Tutorial
                            If you…

                            Are not sure where to start learning JavaScript.
                            Are frustrated with copy-n-paste JavaScript code from others without really understanding it.
                            Cannot add richer and more compelling features to your websites and web applications using JavaScript because you don’t know how to get much out of the language.
                            <br></br>
                            <Button className="d-flex align-items-center justify-content-center" href="https://www.javascripttutorial.net/" variant="link"> Read More..</Button>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Col>

                <Col xs={12} md={4}>
                    <Accordion className="p-3">
                        <Accordion.Item eventKey="2">
                            <Accordion.Header>PHP</Accordion.Header>
                            <Accordion.Body>
                            The PHP team is pleased to announce the release of PHP 8.2.0, RC 2. This is the second release candidate, continuing the PHP 8.2 release cycle, the rough outline of which is specified in the PHP Wiki.
                            <br></br>
                            <Button className="d-flex align-items-center justify-content-center" href="https://www.php.net/" variant="link"> Read More..</Button>
                            </Accordion.Body>
                        </Accordion.Item>
                        </Accordion>
                </Col>
            </Row>
  );
}

