import { useState, useEffect } from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

    const { name, description, price, _id} = courseProp

    // state hook to store the sate of enrollees
    const [count, setCount] = useState(0)
    const [seats, setSeats] = useState(10)
    
    function enroll() {
        // if(seats > 0 && count < 30){
        if(seats > 0){
          setCount(count + 1)
          console.log('Count: ' + count)
          setSeats(seats -1)
          console.log('Seats: ' + seats)
        } //else{
           // return alert('No more seats');
        //}
    };
    
    useEffect(() => {
        if(seats === 0){
            alert("No more seats available.")
        }
    }, [seats])

    return (
        <Row className="my-3">
            <Col Col xs={12} md={6}>
                <Card>
                    <Card.Header>{name}</Card.Header>
                    <Card.Body>
                        <Card.Title>Description</Card.Title>
                        <Card.Text>
                            {description}
                        </Card.Text>

                        <Card.Text>
                            Price:<br></br>
                            {price}
                        </Card.Text>
                        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}